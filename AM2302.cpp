#include "AM2302.hpp"

AM2302::AM2302()
    : dataPin(0), valid(false), humidity(0.0f), temperature(0.0f)
{
  // Empty
}

AM2302::~AM2302()
{
  // Empty
}

void AM2302::readSensorData()
{
  uint8_t sensorData[5];

  // Start
  sendStartSequence();

  valid = false;  // Data is not valid yet
  uint8_t readCount = 0;
  uint8_t saveCount = 0;
  uint8_t lastState = HIGH;

  while (1)
  {
    // Counting us..
    uint8_t usCounter = 0;

    while (digitalRead(dataPin) == lastState)
    {
      delayMicroseconds(1);
      usCounter++;

      // Ensure the data transmission
      // has finished
      if (usCounter == 255)
        break;
    }

    // Quit the loop..
    if (usCounter == 255)
      break;

    // Read bus state
    lastState = digitalRead(dataPin);

    /*
     * Ignore the first 3 transitions
     * of the start sequence
     * Sensor ACK:
     * - sensor LOW
     * - sensor HIGH
     * Sensor DATA:
     * - sensor LOW
     * - ..data.. (then check every even
     * bus state, the odd one's are always 0)
     */
    if ((readCount >= 4) && (readCount % 2 == 0))
    {
      // Shift the pattern to the left by 1 to
      // create space for the new data bit
      sensorData[saveCount / 8] <<= 1;

      // Check if the data bit from the sensor
      // was a "1" (default: 0)
      // I have no fucking idea why you have
      // to check usCounter > 16
      if (usCounter > 16)
        sensorData[saveCount / 8] |= 1;

      saveCount++;
    }

    readCount++;
  }

  // Check if all data bits have been read
  if (saveCount >= 40 && validateChecksum(sensorData))
  {
    uint16_t data[2];

    // Merge bytes
    // Byte 0 + Byte 1 = Humidity
    data[0] = (sensorData[0] << 8) | sensorData[1];

    // Byte 2 + Byte 3 = Temperature
    data[1] = ((sensorData[2] & 0x7F) << 8) | sensorData[3];

    // Calculate real values
    humidity = static_cast<float>(data[0]) / 10.0f;
    temperature = static_cast<float>(data[1]) / 10.0f;

    if ((sensorData[2] & 0x80) != 0)
      temperature *= -1;

    valid = true;
  }
}

void AM2302::sendStartSequence()
{
  // -- Safety --
  // Set bus to HIGH
  pinMode(dataPin, OUTPUT);
  digitalWrite(dataPin, HIGH);
  delay(10);

  // Start sequence
  digitalWrite(dataPin, LOW);
  delay(20);
  //digitalWrite(dataPin, HIGH); // Not needed because sensor got a pullup  
  //delayMicroseconds(40);

  // Set pin mode to input
  pinMode(dataPin, INPUT);
}

bool AM2302::validateChecksum(uint8_t sensorData[])
{
  uint8_t calculatedChecksum = sensorData[0] + sensorData[1] + sensorData[2]
      + sensorData[3];

  if (sensorData[4] == (calculatedChecksum & 0xFF))
    return true;

  return false;
}

bool AM2302::dataIsValid()
{
  return valid;
}

void AM2302::setDataPin(uint8_t dataPin)
{
  this->dataPin = dataPin;
}

float AM2302::getHumidity()
{
  return humidity;
}

float AM2302::getTemperature()
{
  return temperature;
}
