#include "AM2302.hpp"
#include <iostream>
#include <iomanip>
#include <stdlib.h>

int main(int argc, char **argv)
{
  // Setup wiringPi
  wiringPiSetup();

  AM2302 sensor;
  sensor.setDataPin(0);

  while (true)
  {
    sensor.readSensorData();

    while (!sensor.dataIsValid())
      sensor.readSensorData();

    std::cout << "Humidity: " << std::fixed << std::setprecision(1)
        << sensor.getHumidity() << " %" << " | " << "Temperature: "
        << std::fixed << std::setprecision(1) << sensor.getTemperature()
        << " Degree" << std::endl;

    delay(1000);
  }

  return 0;
}
