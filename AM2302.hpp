#ifndef AM2302_HPP_
#define AM2302_HPP_

#include <wiringPi.h>
#include <stdint.h>

class AM2302
{
  private:
    uint8_t dataPin;
    bool valid;
    float humidity;
    float temperature;

    void sendStartSequence();
    bool validateChecksum(uint8_t sensorData[]);
  public:
    AM2302();
    virtual ~AM2302();

    void readSensorData();
    bool dataIsValid();
    void setDataPin(uint8_t dataPin);
    float getHumidity();
    float getTemperature();
};

#endif /* AM2302_HPP_ */
