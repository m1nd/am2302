CC      = g++
CFLAGS  = -Wall -g
LDFLAGS = -L/usr/local/libs -lwiringPi

OBJ = main.o AM2302.o

all: $(OBJ)
	$(CC) $(CFLAGS) -o am2302 $(OBJ) $(LDFLAGS)

clean:
	rm -rf am2302 $(OBJ)